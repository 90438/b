<?php
/**
 * Created by PhpStorm.
 * User: 1
 * Date: 25.10.16
 * Time: 0:42
 */

namespace App\lib;


class Slave
{

    /**
     * @var SlaveScheduler
     */
    private $scheduler;
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $nickName;
    /**
     * @var string
     */
    private $gender;
    /**
     * @var int
     */
    private $age;
    /**
     * @var double
     */
    private $weight;
    /**
     * @var string
     */
    private $skinColor;
    /**
     * @var string
     */
    private $origin;
    /**
     * @var string
     */
    private $description;
    /**
     * @var int
     */
    private $rentPrice;
    /**
     * @var int
     */
    private $fullPrice;

    /**
     * @return string
     */
    public function getNickName()
    {
        return $this->nickName;
    }

    /**
     * @param string $nickName
     * @return $this
     */
    public function setNickName($nickName)
    {
        $this->nickName = $nickName;
        return $this;
    }

    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param string $gender
     * @return $this
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return int
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param int $age
     * @return $this
     */
    public function setAge($age)
    {
        $this->age = $age;
        return $this;
    }

    /**
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @return $this
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return string
     */
    public function getSkinColor()
    {
        return $this->skinColor;
    }

    /**
     * @param string $skinColor
     * @return $this
     */
    public function setSkinColor($skinColor)
    {
        $this->skinColor = $skinColor;
        return $this;
    }

    /**
     * @return string
     */
    public function getOrigin()
    {
        return $this->origin;
    }

    /**
     * @param string $origin
     * @return $this
     */
    public function setOrigin($origin)
    {
        $this->origin = $origin;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return int
     */
    public function getRentPrice()
    {
        return $this->rentPrice;
    }

    /**
     * @param int $rentPrice
     * @return $this
     */
    public function setRentPrice($rentPrice)
    {
        $this->rentPrice = $rentPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getFullPrice()
    {
        return $this->fullPrice;
    }

    /**
     * @param int $fullPrice
     * @return $this
     */
    public function setFullPrice($fullPrice)
    {
        $this->fullPrice = $fullPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return SlaveScheduler
     */
    public function getScheduler()
    {
        return $this->scheduler;
    }

    /**
     * @param SlaveScheduler $scheduler
     */
    public function setScheduler($scheduler)
    {
        $this->scheduler = $scheduler;
    }


}