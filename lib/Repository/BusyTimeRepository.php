<?php
/**
 * Created by PhpStorm.
 * User: Nat
 * Date: 28.10.2016
 * Time: 11:45
 */

namespace App\lib\Repository;


class BusyTimeRepository
{
    /**
     * Возвращает информацию о занятости раба из базы данных по его ID
     * @return array
     */
    public function getBusyPeriods($slaveID)
    {
        return [
            ['id' => 1, 'slave_id' => 1, 'from' => '2016-10-28 10:00:00', 'to' => '2016-10-28 15:00:00'],
            ['id' => 2, 'slave_id' => 1, 'from' => '2016-10-28 22:00:00', 'to' => '2016-10-28 23:00:00']];
    }

}