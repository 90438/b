<?php
/**
 * Created by PhpStorm.
 * User: Nat
 * Date: 28.10.2016
 * Time: 10:26
 */

namespace App\lib;


use Carbon\Carbon;

class BusyTime
{
    /**
     * @var array
     */
    private $data = [];

    /**
     * Check date if it busy or not
     * @param Carbon $date
     * @return bool
     */
    public function check(Carbon $date)
    {
        return !isset($this->data[$date->year][$date->month][$date->day][$date->hour]);
    }

    public function setData($from, $to)
    {
        $fromDate = (new Carbon())->createFromFormat('Y-m-d H:i:s', $from);
        $toDate = (new Carbon())->createFromFormat('Y-m-d H:i:s', $to);
        while (!$fromDate->eq($toDate)) {
            $this->data[$fromDate->year][$fromDate->month][$fromDate->day][$fromDate->hour] = 1;
            $fromDate->addHour();
        }
    }

    public function getData()
    {
        return $this->data;
    }
}