<?php
namespace App\lib;

/**
 * Created by PhpStorm.
 * User: A
 * Date: 27.10.2016
 * Time: 19:17
 */
use Carbon\Carbon;

class SlaveScheduler
{
    /**
     * @var BusyTime
     */
    private $busyTime;
    /**
     * @var Carbon
     */
    private $date;
    /**
     * @var Period
     */
    private $period;

    /**
     * SlaveScheduler constructor.
     * @param BusyTime $busyTime
     * @param Carbon $date
     * @param Period $period
     */
    public function __construct(BusyTime $busyTime = null, Carbon $date = null, Period $period = null)
    {
        $this->busyTime = $busyTime ? $busyTime : new BusyTime();
        $this->date = $date ? $date : new Carbon();
        $this->period = $period ? $period : new Period();
    }


    /**
     * Вовращает ближайший свободный период
     * @param int $maxPeriodLength
     * @return Period
     * @throws \Exception
     */
    public function getClosestFreePeriod($maxPeriodLength = 3)
    {
        $date = $this->date;
        $period = $this->period;
        $i = 0;
        $starDay = $date->day;

        while (true && $i < 1000) {
            if ($this->busyTime->check($date)) {
                if (is_null($period->getStart())) {
                    $period->setStart(clone($date));
                }
                $period->setEnd($date);
                if ($period->getLengthInHours() == $maxPeriodLength) {
                    return $period;
                }
                if ($date->day == $starDay) {
                    if ($period->getLengthInHours() == SlaveRentValidator::MAX_HOURS_PER_DAY) {
                        return $period;
                    }
                }
            } else {
                if ($period->getStart()) {
                    $period->setEnd($date);
                    return $period;
                }
            }
            $date->addHour();
            $i++;
        }

        throw new \Exception('Невозможно найти свободное время. Это какой-то баг');
    }

    /**
     * Проверяет диапазон от from до to на предмет занятости
     * Если хотя бы один час из периода занят - возвращает false
     * @param Carbon $from
     * @param Carbon $to
     * @return array|bool
     */
    public function isFree(Carbon $from, Carbon $to)
    {
        $busyDates = [];
        while (!$from->eq($to)) {
            if (!$this->busyTime->check($from)) {
                $busyDates[] = $from;
            }
            $from->addHour();
        }
        return !empty($busyDates) ? $busyDates : true;

    }

    /**
     * @param BusyTime $busyTime
     */
    public function setBusyTime($busyTime)
    {
        $this->busyTime = $busyTime;
    }


}