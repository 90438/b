<?php
/**
 * Created by PhpStorm.
 * User: Nat
 * Date: 28.10.2016
 * Time: 10:35
 */

namespace App\lib;


use Carbon\Carbon;

class Period
{
    /**
     * @var Carbon
     */
    private $start;
    /**
     * @var Carbon
     */
    private $end;

    /**
     * @return Carbon
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param Carbon $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return Carbon
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param Carbon $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return int
     */
    public function getLengthInHours()
    {
        return $this->end->diffInHours($this->start);
    }
}