<?php
/**
 * Created by PhpStorm.
 * User: Nat
 * Date: 28.10.2016
 * Time: 12:28
 */

namespace App\lib;


use Carbon\Carbon;

class SlaveRentValidator
{
    const MAX_HOURS_PER_DAY = 16;

    private $errors = [];

    public function validate(Slave $slave, Carbon $from, Carbon $to)
    {
        $this->errors = [];

        if (!$to->gt($from)) {
            $this->errors[] = 'Конечная дата аренды не может быть меньше начальной';
            return false;
        }

        if ($from->day == $to->day) {
            if ($to->diffInHours($from) > self::MAX_HOURS_PER_DAY) {
                $this->errors[] = 'Раб не может работать более 16 часов в сутки';
                return false;
            }
        }

        $check = $slave->getScheduler()->isFree($from, $to);

        if (is_array($check)) {
            $msg = [];
            /** @var Carbon $item */
            foreach ($check as $item) {
                $msg[] = $item->format('d.m.Y H:00');
            }
            $this->errors[] = sprintf('Бронирование на запрашиваемый период невозможно. Заняты следующие часы: %s', implode(', ', $msg));
        }

        return empty($this->errors);
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }

}