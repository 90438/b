<?php
namespace App\lib\Factory;

use App\lib\BusyTime;
use App\lib\Repository\SlaveRepository;
use App\lib\Slave;
use App\lib\SlaveScheduler;

/**
 * Created by PhpStorm.
 * User: Nat
 * Date: 28.10.2016
 * Time: 11:58
 */
class SlaveFactory
{
    /**
     * @var SlaveRepository
     */
    private $repo;


    /**
     * SlaveFactory constructor.
     * @param SlaveRepository $repo
     */
    public function __construct(SlaveRepository $repo)
    {
        $this->repo = $repo;
    }

    /**
     * @param $slaveID
     * @return Slave|null
     */
    public function make($slaveID)
    {
        $slaveData = $this->repo->getSlaveByID($slaveID);
        if ($slaveData) {
            $s = new Slave();
            $s->setId($slaveData['id'])
              ->setAge($slaveData['age'])
              ->setDescription($slaveData['description'])
              ->setGender($slaveData['gender'])
              ->setNickName($slaveData['nickname'])
              ->setSkinColor($slaveData['skin_color'])
              ->setRentPrice($slaveData['rent_price'])
              ->setWeight($slaveData['weight']);
            $bt = new BusyTime();
            if (!empty($slaveData['busy'])) {
                foreach ($slaveData['busy'] as $row) {
                    $bt->setData($row['from'], $row['to']);
                }
            }

            $scheduler = new SlaveScheduler($bt);
            $s->setScheduler($scheduler);

            return $s;
        }

        return null;
    }
}