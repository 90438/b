<?php
/**
 * Created by PhpStorm.
 * User: Nat
 * Date: 28.10.2016
 * Time: 12:17
 */

namespace App\lib;


use App\lib\Repository\SlaveRepository;
use Carbon\Carbon;

class RentHandler
{
    /**
     * @var Slave
     */
    private $slave;
    /**
     * @var Carbon
     */
    private $from;
    /**
     * @var Carbon
     */
    private $to;
    /**
     * @var array
     */
    private $errors = [];
    /**
     * @var SlaveRentValidator
     */
    private $validator;

    /**
     * @var SlaveRepository
     */
    private $repo;

    /**
     * RentHandler constructor.
     * @param SlaveRentValidator $validator
     * @param SlaveRepository $repo
     */
    public function __construct(SlaveRentValidator $validator = null, SlaveRepository $repo = null)
    {
        $this->validator = $validator ? $validator : new SlaveRentValidator();
        $this->repo = $repo ? $repo : new SlaveRepository();
    }


    public function rent(Slave $slave, $fromString, $toString)
    {
        $from = (new Carbon())->createFromFormat('Y-m-d H:i:s', $fromString);
        $to = (new Carbon())->createFromFormat('Y-m-d H:i:s', $toString);
        if (!$this->validator->validate($slave, $from, $to)) {
            return false;
        }

        $this->repo->persistRentPeriod($slave->getId(), $from->format('Y-m-d H:i:s'), $to->format('Y-m-d H:i:s'));

        return true;

    }

    public function getErrors()
    {
        return $this->validator->getErrors();
    }


}