<?php
/**
 * Created by PhpStorm.
 * User: A
 * Date: 28.10.2016
 * Time: 10:22
 */

require_once('vendor/autoload.php');

showSlave();

/**
 * эта функция имитирует контроллер и обрабатывает принятие запроса на показ страницы раба.
 * выводится инфа о рабе и ближайший свободнй период
 */
function showSlave()
{
    $slaveFactory = new \App\lib\Factory\SlaveFactory(new \App\lib\Repository\SlaveRepository());

    //здесь получаем id выбранного на фронте раба из вебзапроса (объект Request)
    //например $request->query->get('slave_id');

    $id = 1;

    $slave = $slaveFactory->make($id);
    if (!$slave) {
        throw new Exception('404 - Slave not found!');
    }

    //здесь рендерим шаблон с данными раба
    //return $this->render('template.twig,['slave'=>$slave, 'closestFreePeriod' => $slave->getScheduler()->getClosestFreePeriod(3)])
    var_dump($slave->getScheduler()->getClosestFreePeriod(3));
    return ['slave' => $slave, 'closestFreePeriod' => $slave->getScheduler()->getClosestFreePeriod(3)];

}

/**
 * эта функция имитирует контроллер и обрабатывает принятие запроса на бронирование раба.
 */
function rentSlave()
{
    $slaveFactory = new \App\lib\Factory\SlaveFactory(new \App\lib\Repository\SlaveRepository());
    $rentHandler = new \App\lib\RentHandler();
    $from = '2016-10-28 10:00:00';
    $to = '2016-10-28 16:00:00';
    $id = 1;
    $slave = $slaveFactory->make($id);
    if (!$slave) {
        throw new Exception('404 - Slave not found!');
    }

    if (!$rentHandler->rent($slave, $from, $to)) {
        //отправляем на фронт массив ошибок
        var_dump(['errors' => $rentHandler->getErrors()]);
        die;
    }

    echo  'success';
}