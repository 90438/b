<?php
use App\lib\SlaveScheduler;

/**
 * Created by PhpStorm.
 * User: Nat
 * Date: 28.10.2016
 * Time: 10:44
 */
class SlaveSchedulerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \App\lib\BusyTime
     */
    private $busyTime;

    public function setUp()
    {
        $this->busyTime = new \App\lib\BusyTime();
    }

    public function testGetClosestPeriodMethod()
    {
        $this->busyTime->setData('2016-10-28 8:00', '2016-10-28 14:00');
        $scheduler = new SlaveScheduler($this->busyTime);

        $p = $scheduler->getClosestFreePeriod();

        $this->assertEquals($p->getStart()->format('Y-m-d H'), '2016-10-28 14');
        $this->assertEquals($p->getEnd()->format('Y-m-d H'), '2016-10-28 17');
        $p = $scheduler->getClosestFreePeriod(5);
        $this->assertEquals($p->getStart()->format('Y-m-d H'), '2016-10-28 14');
        $this->assertEquals($p->getEnd()->format('Y-m-d H'), '2016-10-28 19');

    }

    public function testWeCantRentSlaveMore16HoursInOneDay()
    {
        $this->busyTime->setData('2016-10-28 01:00', '2016-10-28 02:00');
        $scheduler = new SlaveScheduler($this->busyTime, (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 01'));
        $p = $scheduler->getClosestFreePeriod(20);
        $this->assertEquals($p->getStart()->format('Y-m-d H'), '2016-10-28 02');
        $this->assertEquals($p->getEnd()->format('Y-m-d H'), '2016-10-28 18');
    }

    public function testWeCanRentSlaveMore16HoursOnDifferentDays()
    {
        $this->busyTime->setData('2016-10-28 01:00', '2016-10-28 02:00');
        $scheduler = new SlaveScheduler($this->busyTime, (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 20'));
        $p = $scheduler->getClosestFreePeriod(20);
        $this->assertEquals($p->getStart()->format('Y-m-d H'), '2016-10-28 20');
        $this->assertEquals($p->getEnd()->format('Y-m-d H'), '2016-10-29 16');
    }

    public function testIsFreeMethod()
    {
        $this->busyTime->setData('2016-10-28 11:00', '2016-10-28 17:00');
        $scheduler = new SlaveScheduler($this->busyTime, (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 20'));
        $from = (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 13');
        $to = (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 16');
        $this->assertNotEmpty($scheduler->isFree($from, $to));
        $from = (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 17');
        $to = (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 18');
        $this->assertTrue($scheduler->isFree($from, $to));

    }
}
