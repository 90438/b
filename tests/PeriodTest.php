<?php

/**
 * Created by PhpStorm.
 * User: Nat
 * Date: 28.10.2016
 * Time: 11:15
 */
class PeriodTest extends PHPUnit_Framework_TestCase
{
    public function testGetLengthInHoursMethod()
    {
        $p = new \App\lib\Period();
        $p->setStart((new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 8'));
        $p->setEnd((new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 17'));
        $this->assertEquals(9, $p->getLengthInHours());

    }
}
