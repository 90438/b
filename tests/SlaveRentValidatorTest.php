<?php

/**
 * Created by PhpStorm.
 * User: Nat
 * Date: 28.10.2016
 * Time: 12:48
 */
class SlaveRentValidatorTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \App\lib\SlaveRentValidator
     */
    private $validator;
    /**
     * @var \App\lib\Slave
     */
    private $slave;


    public function setUp()
    {
        $returnData = [
            (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 11'),
            (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 12'),
            (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 13'),
            (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 14')];
        $scheduler = $this->getMockBuilder(\App\lib\SlaveScheduler::class)
                          ->disableOriginalConstructor()
                          ->getMock();
        $scheduler->method('isFree')->willReturn($returnData, true, false, false);
        $this->slave = new \App\lib\Slave();
        $this->slave->setScheduler($scheduler);
        $this->validator = new \App\lib\SlaveRentValidator();
    }

    public function testValidatorMethod()
    {
        $from = (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 17');
        $to = (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 18');
        $res = $this->validator->validate($this->slave, $from, $to);
        $this->assertFalse($res);
        $res = $this->validator->validate($this->slave, $from, $to);
        $this->assertTrue($res);
        $this->assertEmpty($this->validator->getErrors());

    }

    public function testFromGreaterThenTo()
    {

        $from = (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 24');
        $to = (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 23');
        $res = $this->validator->validate($this->slave, $from, $to);
        $this->assertFalse($res);
        $this->assertNotEmpty($this->validator->getErrors());

    }

    public function testLimitWorkingHoursPerDay()
    {

        $from = (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 1');
        $to = (new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 23');
        $res = $this->validator->validate($this->slave, $from, $to);
        $this->assertFalse($res);
        $this->assertNotEmpty($this->validator->getErrors());

    }
}
