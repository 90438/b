<?php

/**
 * Created by PhpStorm.
 * User: Nat
 * Date: 28.10.2016
 * Time: 10:50
 */
class BusyTimeTest extends PHPUnit_Framework_TestCase
{
    public function testCheckMethod()
    {
        $b = new \App\lib\BusyTime();
        $b->setData('2016-10-28 8:00', '2016-10-28 14:00');
        $check = $b->check((new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 10'));
        $this->assertFalse($check);
        $check = $b->check((new \Carbon\Carbon())->createFromFormat('Y-m-d H', '2016-10-28 14'));
        $this->assertTrue($check);

    }

    public function testSetDataMethod()
    {
        $b = new \App\lib\BusyTime();
        $b->setData('2016-10-28 8:00', '2016-10-28 14:00');
        $exp = [2016 => [10 => [28 => [8 => 1, 9 => 1, 10 => 1, 11 => 1, 12 => 1, 13 => 1]]]];
        $this->assertEquals($exp, $b->getData());

    }
}
